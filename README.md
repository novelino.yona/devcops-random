To use this pipeline properly please fill the variables below:
    1. Key ID_RSA - Type File - Value Private key for accessing the destination EC2 server where nginx will be deployed - Protected.
    2. Key SERVER_IP - Type Variable - Value destination EC2 server IP where nginx will be deployed - Protected and Masked.
    3. Key SERVER_USER - Type Variable - Value user with proper permission to docker inside destination EC2 server - Protected and Masked. 
