#Create keypair
resource "aws_key_pair" "bibit-kp" {
  key_name   = "bibit-kp"
  public_key = var.pub_key
}

#Create security groups
resource "aws_security_group" "bibit-sg" {
  name   = "bibit-sg" 
  vpc_id = aws_vpc.Main.id
  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr
      description = ingress.value.description
    }
  }

  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
      description = "Allow all traffic to internet"
    }
}

#Create Launch Template
resource "aws_launch_template" "bibit_lt" {
  name_prefix          = "bibit_lt"
  image_id             = data.aws_ami.ubuntu.id
  instance_type        = var.instance_type
  key_name             = aws_key_pair.bibit-kp.key_name
  network_interfaces {
    subnet_id = aws_subnet.privatesubnets.id
    security_groups = [aws_security_group.bibit-sg.id]
  }
}

#Create Auto Scaling Group
resource "aws_autoscaling_group" "bibit_asg" {
  name               = "bibit_asg"
  availability_zones = [aws_subnet.privatesubnets.availability_zone]
  desired_capacity   = 2
  max_size           = 5
  min_size           = 2
  #vpc_zone_identifier = [aws_subnet.privatesubnets.id]
  launch_template {
    id      = aws_launch_template.bibit_lt.id
    version = "$Latest"
  }
  tag {
    key = "name"
    value = "bibit-asg"
    propagate_at_launch = true
  }
}

#Create Auto Scaling Group Policy
resource "aws_autoscaling_policy" "bibit_asg_policy" {
  name                   = "bibit_asg_policy"
  autoscaling_group_name = aws_autoscaling_group.bibit_asg.name
  policy_type            = "TargetTrackingScaling"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 45.0
  }
}