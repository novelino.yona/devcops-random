There are some notes before using this Terraform HCL module:
    1. Go to terraform.tfvars and change the value of secret_key, access_key, and region according to requirement or destination account.
    2. CIDR variable from tfvars can be edited following the requirements.
    3. You can apply the module after change the secret_key and access_key.
    4. I have attached the private key for pub_key variable, but if you have your own private key you can just change the pub_key variable.
    5. The security group rules is allowing SSH from inside VPC, allowing HTTP from everywhere (just in case want to connect it to ALB), and allowing all traffic to anywhere.
    6. AMI used on this Terraform module is Ubuntu 20.04.