variable "region" {}
variable "secret_key" {}
variable "access_key" {}
variable "main_vpc_cidr" {}
variable "public_subnets" {}
variable "private_subnets" {}
variable "instance_type" {}
variable "pub_key" {}
variable "ingress_rules" {
  type = list(object({
    from        = number
    to          = number
    protocol    = string
    cidr        = list(string)
    description = string
  }))
  default = [ {
    cidr = ["172.16.0.0/24"]
    from = 22
    protocol = "tcp"
    to = 22
    description = "Allow SSH from Main VPC"
},
{ cidr = ["0.0.0.0/0"]
  from = 80
  protocol = "tcp"
  to = 80
  description = "Allow HTTP Access from everywhere"
  } ]
}
