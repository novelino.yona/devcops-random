main_vpc_cidr = "172.16.0.0/24"
public_subnets = "172.16.0.0/25"
private_subnets = "172.16.0.128/25"
region = "ap-southeast-1" #Please change this accordingly to destinated region
secret_key = "" #Please change this accordingly to the user secret key
access_key = "" #Please change this accordingly to the user access key
instance_type = "t2.medium"
pub_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAjbtsJnO9h5QgN5KBHr5FOTNFx3dMwCc5XM39HkCKORP64HdeNjcRSu9OH85SmAprKrI8ADYoTsB6Zrbg2OifCxWcIhufgQSQ3KpahnkQ6b6/Vz4OfDC1kljEMSqYmGjyjyMqOCAcqoIR7l7QIv8gKZbI0pznJkbKJ6LGs2oErIKCq7gPvtIrww5egAQTv15bKlbHy0Pvj/OseiGWSyO67EHoBrYzu99FLrYPa05/LZwlc5Akl7gWZ1qupu70vUrTO/kj4xrWr/QgoSRtEQq629QLDgftExLcK9gJ0BxSo72PGQxTrIadIefywJBwHyW8hj1/nrJ6a/w1nxSSBpxhew== rsa-key-20211020"
